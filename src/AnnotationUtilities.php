<?php


namespace Develia\Symfony;


use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\AnnotationReader;


abstract class AnnotationUtilities
{
    private function __construct()
    {

    }

    /**
     * @param object | string $classOrObject
     * @param string $method
     * @param string $annotationClass
     * @return Annotation|null
     */
    public static function getMethodAnnotation(object|string $classOrObject, string $method, string $annotationClass)
    {

        if (is_object($classOrObject))
            $classOrObject = get_class($classOrObject);

        try {
            $method = new \ReflectionMethod($classOrObject, $method);
            $reader = new AnnotationReader();
            return $reader->getMethodAnnotation($method, $annotationClass);
        } catch (\ReflectionException $e) {
            return null;
        }

    }

    /**
     * @param object | string $classOrObject
     * @param string $annotationClass
     * @return Annotation|null
     */
    public static function getClassAnnotation(object|string $classOrObject, string $annotationClass)
    {

        if (is_object($classOrObject))
            $classOrObject = get_class($classOrObject);

        try {
            $class = new \ReflectionClass($classOrObject);
            $reader = new AnnotationReader();
            return $reader->getClassAnnotation($class, $annotationClass);
        } catch (\ReflectionException $e) {
            return null;
        }

    }


}