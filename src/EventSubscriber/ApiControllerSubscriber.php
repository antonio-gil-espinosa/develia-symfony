<?php

namespace Develia\Symfony\EventSubscriber;

use Develia\Symfony\Controller\ApiController;
use DomainException;
use InvalidArgumentException;
use OutOfBoundsException;
use OutOfRangeException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class ApiControllerSubscriber implements EventSubscriberInterface {

    private SerializerInterface $_serializer;

    public function __construct(SerializerInterface $serializer) {
        $this->_serializer = $serializer;
    }

    public static function getSubscribedEvents(): array {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
            KernelEvents::VIEW      => 'onKernelView',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void {

        $request = $event->getRequest();
        $isApiController = self::_isApiController($request);
        if (!$isApiController)
            return;

        $exception = $event->getThrowable();

        $data = $this->_exceptionToObject($exception);
        $data["error"] = true;

        $event->setResponse(self::_createResponse(
            $request,
            $data,
            self::_getStatusCodeForException($exception),
            $this->_serializer
        ));
    }

    public function onKernelView(ViewEvent $event): void {
        $request = $event->getRequest();
        $isApiController = self::_isApiController($request);
        if (!$isApiController) {
            return;
        }

        if ($event->getResponse() instanceof Response)
            return;

        $event->setResponse(self::_createResponse($request, $event->getControllerResult(), Response::HTTP_OK, $this->_serializer));
    }

    private static function _getStatusCodeForException(Throwable $exception): int {

        switch (true) {
            case $exception instanceof NotFoundHttpException:
                return Response::HTTP_NOT_FOUND; // 404
            case $exception instanceof AccessDeniedHttpException:
                return Response::HTTP_FORBIDDEN; // 403
            case $exception instanceof MethodNotAllowedHttpException:
                return Response::HTTP_METHOD_NOT_ALLOWED; // 405
            case $exception instanceof UnauthorizedHttpException:
                return Response::HTTP_UNAUTHORIZED; // 401
            case $exception instanceof InvalidArgumentException:
            case $exception instanceof OutOfRangeException:
            case $exception instanceof OutOfBoundsException:
            case $exception instanceof DomainException:
                return Response::HTTP_BAD_REQUEST; // 400
            case $exception instanceof ConflictHttpException:
                return Response::HTTP_CONFLICT; // 409
            case $exception instanceof UnprocessableEntityHttpException:
                return Response::HTTP_UNPROCESSABLE_ENTITY; // 422
            case $exception instanceof HttpException:
                return $exception->getStatusCode();

            default:
                return Response::HTTP_INTERNAL_SERVER_ERROR; // 500
        }
    }

    private static function _isApiController(Request $request): bool {

        $controller = $request->attributes->get('_controller');
        if (!$controller)
            return false;

        $parts = explode('::', $controller);
        if (!$parts)
            return false;


        return is_subclass_of($parts[0], ApiController::class);
    }

    private function _exceptionToObject(Throwable $exception): array {

        $previous = $exception->getPrevious();
        return [
            'type'     => get_class($exception),
            'code'     => $exception->getCode(),
            'message'  => $exception->getMessage(),
            'file'     => $exception->getFile(),
            'line'     => $exception->getLine(),
            'previous' => $previous ? self::_exceptionToObject($previous) : null,
        ];
    }

    private function _createResponse(Request $request, mixed $data, int $statusCode): Response {

        $acceptHeader = $request->headers->get('Accept', 'application/json');
        $format = stripos($acceptHeader, 'application/xml') !== false ? 'xml' : 'json';
        $content = $this->_serializer->serialize($data, $format);
        return new Response($content, $statusCode, ['Content-Type' => 'application/' . $format]);

    }

}