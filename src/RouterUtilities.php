<?php


namespace Develia\Symfony;


use Symfony\Component\Routing\Annotation\Route;


abstract class RouterUtilities
{
    private function __construct()
    {

    }

    /**
     * @param string | object $controller
     * @param string | null $action
     * @return null | string
     */
    public static function getRoutePath($controller, $action = null)
    {

        $path = "";

        /** @var Route $controllerRoute */
        $controllerRoute = AnnotationUtilities::getClassAnnotation($controller, Route::class);
        if ($controllerRoute) {
            $path .= rtrim($controllerRoute->getPath(), "/");
        }

        if ($action) {

            /** @var Route $actionRoute */
            $actionRoute = AnnotationUtilities::getMethodAnnotation($controller, $action, Route::class);
            if ($actionRoute) {
                $path .= $actionRoute->getPath();
            }


        }

        return $path ?: null;

    }


    /**
     * @param string | object $controller
     * @param string | null $action
     * @return null | string
     */
    public static function getRouteName($controller, $action = null)
    {
        /** @var Route $route */
        $route = AnnotationUtilities::getMethodAnnotation($controller, $action, Route::class);
        if ($route) {

            $name = $route->getName();
            if ($name) {
                return $name;
            }

            $output = strtolower(is_string($controller) ? $controller : get_class($controller));
            $output = strtolower($output);
            $output = str_replace("controller", "", $output);
            $output = str_replace("\\", "_", $output);
            $output = str_replace("__", "_", $output);

            return $output . "_" . $action;
        }

        return null;


    }


}