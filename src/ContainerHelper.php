<?php

namespace Develia\Symfony;

use Symfony\Component\DependencyInjection\ContainerBuilder;

abstract class ContainerHelper
{
    private function __construct()
    {
    }


    public static function getDefinitionsSubclassing(ContainerBuilder $container, $class){
        $definitions = $container->getDefinitions();
        return array_values(array_filter($definitions,function($x) use ($class) {
            return is_subclass_of($x->getClass(),$class) && !$x->isAbstract();
        }));
    }
}