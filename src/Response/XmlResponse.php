<?php

namespace Develia\Symfony\Response;

use Symfony\Component\HttpFoundation\Response;

class XmlResponse extends Response {


    public function __construct($data, $statusCode, $rootElement = "root") {
        parent::__construct(self::_toXml($data, $rootElement), $statusCode, ['Content-Type' => 'application/xml']);
    }

    private static function _toXml($objeto, $rootElement = "root", $xml = null) {
        // Crear un nuevo SimpleXMLElement si no se pasa uno
        if ($xml === null) {
            $xml = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><$rootElement></$rootElement>");
        }

        foreach ($objeto as $key => $value) {
            // Si el valor es un array o un objeto, llamar recursivamente
            if (is_array($value) || is_object($value)) {
                $subnode = $xml->addChild($key);
                self::_toXml($value, $key, $subnode);
            } else {
                // Agregar el valor como un nodo hijo
                $xml->addChild($key, htmlspecialchars($value));
            }
        }

        return $xml->asXML();
    }

}