<?php


namespace Develia\Symfony\Controller;


use Develia\Symfony\RouterUtilities;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

abstract class BaseCrudController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    protected $repository;


    private $router;


    public function __construct(EntityManagerInterface $em)
    {

        $this->em = $em;
        $this->repository = $em->getRepository($this->entityClass());

    }

    protected abstract function entityClass(): string;


    /**
     * @Route("/query")
     */
    public function query(): Response
    {
        return $this->json($this->repository->findAll());
    }


    /**
     * @Route("/")
     */
    public function index(): Response
    {


        return $this->render($this->indexView(), [
            "model" => $this->repository->findAll(),
            "queryRoute" => RouterUtilities::getRouteName($this, "query"),
            "creationRoute" => RouterUtilities::getRouteName($this, "create"),
            "detailsRoute" => RouterUtilities::getRouteName($this, "single"),
            "deleteRoute" => RouterUtilities::getRouteName($this, "delete"),
            "columns" => $this->columns()
        ]);
    }

    /**
     * @return string
     */
    protected abstract function indexView(): string;

    protected abstract function columns(): array;

    /**
     * @Route("/new")
     */
    public function create(Request $request): Response
    {
        $errors = [];
        $class = $this->entityClass();
        $entity = new $class();

        $form = $this->createForm($this->formClass(), $entity);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $errors = $this->validate($form, $entity, false);
                if (!$errors) {
                    $this->onPersisting($form, $entity, false);
                    $this->em->persist($entity);
                    $this->em->flush();
                    $this->onPersisted($form, $entity, false);

                    return $this->redirectToRoute(RouterUtilities::getRouteName($this, "single"), [
                        "id" => $entity->getId()
                    ]);
                } else {

                    foreach ($errors as $error) {
                        $form->addError(new FormError($error));
                    }

                }

            }
        }


        return $this->render($this->singleView(), [
            "form" => $form->createView(),
            "errors" => $errors
        ]);
    }

    protected abstract function formClass(): string;

    /**
     * @param FormInterface $form
     * @param $entity
     * @param bool $updating
     */
    protected function validate(FormInterface $form, $entity, bool $updating)
    {
        return [];
    }

    /**
     * @param FormInterface $form
     * @param $entity
     * @param bool $updating
     */
    protected function onPersisting(FormInterface $form, $entity, bool $updating)
    {

    }

    /**
     * @param FormInterface $form
     * @param $entity
     * @param bool $updating
     */
    protected function onPersisted(FormInterface $form, $entity, bool $updating)
    {
    }


    /**
     * @return string
     */
    protected abstract function singleView(): string;

    /**
     * @Route("/{id}")
     */
    public function single(Request $request, $id): Response
    {
        $errors = [];
        $entity = $this->em->find($this->entityClass(), $id);
        $form = $this->createForm($this->formClass(), $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $errors = $this->validate($form, $entity, true);
                if (!$errors) {
                    $this->onPersisting($form, $entity, true);
                    $this->em->persist($entity);
                    $this->em->flush();
                    $this->onPersisted($form, $entity, true);
                    return $this->redirectToRoute(RouterUtilities::getRouteName($this, "single"), ["id" => $entity->getId()]);
                } else {
                    foreach ($errors as $error) {
                        $form->addError(new FormError($error));
                    }
                }
            }
        }
        return $this->render($this->singleView(), [
            "form" => $form->createView(),
            "errors" => $errors,
            "creationRoute" => RouterUtilities::getRouteName($this, "create"),
            "detailsRoute" => RouterUtilities::getRouteName($this, "single"),
            "deleteRoute" => RouterUtilities::getRouteName($this, "delete")
        ]);
    }

    /**
     * @Route("/delete/{id}")
     */
    public function delete(Request $request, $id): Response
    {
        $entity = $this->em->find($this->entityClass(), $id);
        $this->em->remove($entity);
        $this->em->flush();

        return $this->redirectToRoute(RouterUtilities::getRouteName($this, "index"));
    }


}