<?php

namespace Develia\Symfony\Controller;

use Develia\Reflector;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class RestController extends ApiController {


    /**
     * @Route("/", methods={"GET"})
     */
    public function getAction(Request $request): JsonResponse {
        return $this->callAction("doGetAction", $request);
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function postAction(Request $request): JsonResponse {
        return $this->callAction("doPostAction", $request);
    }

    /**
     * @Route("/{id}", methods={"PUT"})
     * @noinspection PhpUnusedParameterInspection
     */
    public function putAction($id, Request $request): JsonResponse {
        // Lógica por defecto para manejar la petición PUT
        return $this->callAction("doPutAction", $request);
    }

    /**
     * @Route("/{id}", methods={"DELETE"})
     * @noinspection PhpUnusedParameterInspection
     */
    public function deleteAction($id, Request $request): JsonResponse {
        return $this->callAction("doDeleteAction", $request);
    }


    public function doGetAction(...$args): JsonResponse {
        throw new NotFoundHttpException();
    }

    public function doPostAction(...$args): JsonResponse {
        throw new NotFoundHttpException();
    }

    public function doPutAction($id, ...$args): JsonResponse {
        throw new NotFoundHttpException();
    }

    public function doDeleteAction($id, ...$args): JsonResponse {
        throw new NotFoundHttpException();
    }

    private function callAction($action, Request $request): JsonResponse {
        try {



            $args = $this->argumentResolver->getArguments($request, [$this, $action]);

            if (Reflector::isMethodRedefined($this, $action))

                return new JsonResponse(call_user_func_array([$this, $action], $args));
            return new JsonResponse(null, 404);
        } catch (NotFoundHttpException $nfhex) {
            return new JsonResponse(["message" => $nfhex->getMessage()], 404);
        } catch (\Throwable $exception) {
            return new JsonResponse(["message" => $exception->getMessage()], 500);
        }
    }




}