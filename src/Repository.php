<?php

namespace Develia\Symfony;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

/**
 * Class Repository
 * @package Develia\Symfony
 * @template T
 */
class Repository extends ServiceEntityRepository
{


    /**
     * @param $id
     * @return T
     */
    public function getReference($id) {
        return $this->_em->getReference($this->getClassName(),$id);
    }

    /**
     * Repository constructor.
     * @param ManagerRegistry $registry
     * @param class-string<T> $entityClass
     */
    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    /**
     * Add object to repository
     *
     * @param T $entity Object to be added
     * @param bool $flush Whether to flush after persist or not.
     */
    public function persist($entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Remove object from repository
     *
     * @param T $entity Object to be removed
     * @param bool $flush Whether to flush after removal or not.
     */
    public function remove($entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Find an object by id
     *
     * @param mixed $id
     * @param int|null $lockMode
     * @param int|null $lockVersion
     * @return T|null An object of type T
     */
    public function find($id, $lockMode = null, $lockVersion = null): ?object
    {
        return parent::find($id, $lockMode, $lockVersion);
    }

    /**
     * Find one object by criteria
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @return T|null An object of type T
     */
    public function findOneBy(array $criteria, array $orderBy = null): ?object
    {
        return parent::findOneBy($criteria, $orderBy);
    }

    /**
     * Find all objects
     *
     * @return T[] An array of objects of type T
     */
    public function findAll(): array
    {
        return parent::findAll();
    }

    /**
     * Find objects by criteria
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return T[] An array of objects of type T
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }

}